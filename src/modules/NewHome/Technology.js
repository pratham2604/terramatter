import { Grid, Header, Image, Button } from 'semantic-ui-react';
import Products from './../../assets/Products.png';
import TechnologyImg from './../../assets/Technology.png';
import { Link } from 'react-router-dom';
const Technology = () => (
  <div style={{ padding: '0em', marginBottom: '4em' }} vertical>
    <Grid>
      <Grid.Row centered>
        <Grid.Column width={14}>
          <div style={{ padding: '2em', backgroundColor: 'bisque', borderRadius: '10px' }}>
            <Grid celled='internally' columns='equal' stackable>
              <Grid.Row textAlign='center'>
                <Grid.Column style={{boxShadow: 'none'}}>
                  <Image src={Products} fluid rounded />
                </Grid.Column>
                <Grid.Column verticalAlign='middle' style={{boxShadow: 'none'}}>
                  <Header as={'h3'} style={{ fontSize: '2em' }} className='poppins-font'>
                    What are we up to?
                  </Header>
                  <div style={{ fontSize: '1.33em', textAlign: 'justify' }} className='poppins-font'>
                    We are creating value from emissions &amp; industry side streams for meeting nutrient &amp; food security
                    without adding more carbon emission.
                  </div>
                  <div style={{ marginTop: '2em' }}>
                    <Button as={Link} to="/products" color='blue' className='poppins-font'>View Products</Button>
                  </div>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row textAlign='center' style={{boxShadow: 'none'}}>
                <Grid.Column verticalAlign='middle'>
                  <Header as={'h3'} style={{ fontSize: '2em' }} className='poppins-font'>
                    How are we doing it?
                  </Header>
                  <div style={{ fontSize: '1.33em', textAlign: 'justify' }} className='poppins-font'>
                    Converting the problem into solution
                    We are decarbonizing industries in the process to make alternative ingredients.
                  </div>
                  <div style={{ marginTop: '2em' }}>
                    <Button as={Link} to="/technology" color='blue' className='poppins-font'>View Technology</Button>
                  </div>
                </Grid.Column>
                <Grid.Column style={{boxShadow: 'none'}}>
                  <Image src={TechnologyImg} fluid rounded />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </div>
);



export default Technology;