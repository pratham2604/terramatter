import {
  Container,
  Header,
} from 'semantic-ui-react'

const HomepageHeading = ({ mobile }) => (
  <Container text>
    <Header
      as='h4'
      content='Harnessing Carbon to Enable Circular Economy'
      inverted
      style={{
        fontSize: mobile ? '2em' : '2em',
        fontWeight: 'normal',
        marginBottom: 0,
        marginTop: mobile ? '1.5em' : '3em',
      }}
    />
    <Header
      as='h2'
      content='Terramatter aims at putting Carbon emissions to use to make critical & specialty chemicals like Single cell Proteins (SCP) thereby creating a positive spiral of circular carbon economy.'
      inverted
      style={{
        fontSize: mobile ? '1.5em' : '1.2em',
        fontWeight: 'normal',
        marginTop: mobile ? '0.5em' : '3.5em',
      }}
    />
  </Container>
)

export default HomepageHeading;