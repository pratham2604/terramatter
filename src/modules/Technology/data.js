export const STAGES = [{
  image: 'https://enifer.com/wp-content/uploads/2023/11/technology-1.svg',
  title: 'Stage 1',
  description: 'CO2 fermentation: Co2 is fed to organisms that feed on it and the output of this stage is passed as feed stock to the 2nd stage ',
}, {
  image: 'https://enifer.com/wp-content/uploads/2023/11/technology-2.svg',
  title: 'Stage 2',
  description: 'Fermentation: Here the feed from Co2 fermentation is used as substrate for the organisms that grow in a controlled environment either in pure culture or a consortium.',
}, {
  image: 'https://enifer.com/wp-content/uploads/2023/11/technology-3.svg',
  title: 'Stage 3',
  description: 'Product is then extracted from the rest of the biomass and purified to achieve edible status',
}]