import React from 'react';
import Container from '../Container'
import './index.css';
import { Divider, Grid, Image, Segment } from 'semantic-ui-react';
import { STAGES } from './data';

const index = () => {
  return (
    <Container>
      <div className='technology-heading poppins-font'>TECHNOLOGY – Carbon offset solution for industries</div>
      <div className='technology-description poppins-font'>
        By 2nd domestication of organisms we can create resilience across of the food production system. 
        This could potentially solve the problem of nutrient & food security in a sustainable way without harming the planet.
        Our process is carbon negative, highly scalable. It is independent of rainfall, climate & geography.
      </div>
      <div className='technology-description poppins-font'>
        Terramatter’s flagship product TerraMyco is a form of Mycoprotein. 
        It is derived from generally recognized as Safe (GRAS) organisms which are grown in a carbon negative process. 
        We call this process ALTMATTER. 
        Altmatter is a gaseous fermentation process that uses carbon dioxide as feedstock to be fed to the fermentation chambers. 
        This Co2 undergoes a 2 stage process to produce intended biomass.
      </div>
      <Segment stackable style={{margin: '2rem', padding: '2rem',paddingTop: '4rem', backgroundColor: '#a0a0a0'}}>
        <Grid stackable>
          <Grid.Row centered>
            <Grid.Column width={6}>
              <div className='process-heading poppins-font'>Our process</div>
              <div className='process-description poppins-font'>Learn about our process in detail:</div>
            </Grid.Column>
            <Grid.Column width={10}>
              <Grid stackable>
                {STAGES.map((stage, index) => (
                  <ProcessStage image={stage.image} title={stage.title} description={stage.description} isLastIndex={index === STAGES.length - 1}/>
                ))}
              </Grid>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    </Container>
  );
};

const ProcessStage = ({ image, title, description, isLastIndex }) => (
  <Grid.Row>
    <Grid.Column width={4}>
      <div className='technology-image'>
        <Image src={image}/>
      </div>
    </Grid.Column>
    <Grid.Column width={12}>
      <div className='poppins-font' style={{fontSize: '1.25rem', paddingBottom: '1rem'}}>
        <div style={{ fontWeight: 'bold', fontSize: '1.5rem', marginBottom: '1rem' }}>
         {title}
        </div>
        {description}
      </div>
      {!isLastIndex && <Divider />}
    </Grid.Column>
  </Grid.Row>
)

export default index;