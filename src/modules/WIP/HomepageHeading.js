import PropTypes from 'prop-types'
import React from 'react'
import {
  Container,
  Header
} from 'semantic-ui-react'

/* Heads up!
 * HomepageHeading uses inline styling, however it's not the best practice. Use CSS or styled
 * components for such things.
 */
const HomepageHeading = ({ mobile }) => (
  <Container text>
    <Header
      as='h1'
      content='TerraMatter'
      inverted
      style={{
        fontSize: mobile ? '2em' : '4em',
        fontWeight: 'normal',
        marginBottom: 0,
        marginTop: mobile ? '2.5em' : '3em',
      }}
    >
      <span style={{color: '#a7c090'}}>Terra</span>
      <span style={{color: '#8b8c8e'}}>Matter</span>
    </Header>
    <Header
      as='h2'
      content='Harnessing Carbon to enable Circular economy'
      inverted
      style={{
        fontSize: mobile ? '1em' : '1.7em',
        fontWeight: 'normal',
        marginTop: mobile ? '1em' : '1.5em',
      }}
    />
  </Container>
)

HomepageHeading.propTypes = {
  mobile: PropTypes.bool,
}

export default HomepageHeading;
