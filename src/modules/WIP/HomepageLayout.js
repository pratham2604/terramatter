import React from 'react';
import { Segment, Header, Grid } from 'semantic-ui-react';
import { isMobile } from 'react-device-detect';

const HomepageLayout = () => {
  return (
    <>
      <Segment style={{ padding: '6em 0em' }} vertical>
        <Grid container stackable verticalAlign='middle' centered>
          <Grid.Row>
            <Grid.Column width={10} textAlign='center'>
              <p style={{ fontSize: isMobile ? '1.1em' : '1.33em' }}>
                TerraMatter aims at putting Carbon emissions to use to make specialty chemicals, materials
                 and fuels thereby creating a positive spiral of circular carbon economy
              </p>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
      <Segment style={{ padding: '6em 0em', backgroundColor: '#efefef' }} vertical>
        <Grid container stackable verticalAlign='middle' centered>
        <Grid.Row>
          <Grid.Column width={12} textAlign='center'>
            <Header as='h3' style={{ fontSize: isMobile ? '1.5em' : '2em' }}>
              INSPIRATION
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row textAlign='center'>
          <Grid.Column style={{ paddingBottom: '1em', paddingTop: '1em' }} width={8}>
            <p style={{ fontSize: isMobile ? '1.1em' : '1.33em', textAlign: 'center', color: '#535353', fontStyle: 'italic' }} >
              "The Earth is the only world known so far to harbor life. There is nowhere else,
              at least in the near future, to which our species could migrate. 
              Visit, yes. Settle, not yet. Like it or not, for the moment the Earth is where we make our stand."
            </p>
            <p style={{ fontSize: isMobile ? '1.1em' : '1.33em', textAlign: 'right', color: '#535353' }}>— Carl Sagan, Pale Blue Dot, 1994</p>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={10} textAlign='center'>
            <p style={{ fontSize: isMobile ? '1.1em' : '1.33em' }}>
              Terramatter took this inspiration from Carl Sagan and endeavor to keep our planet overcome the 
              menace of climate change brought by GHG emissions.
            </p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
    <Segment style={{ padding: '6em 0em' }} vertical>
      <Grid container stackable verticalAlign='middle' centered>
        <Grid.Row>
          <Grid.Column width={10} textAlign='center'>
            <Header as='h3' style={{ fontSize: isMobile ? '1.5em' : '2em', paddingBottom: isMobile ? '1.25em' : '2em' }}>
              Mission
            </Header>
            <p style={{ fontSize: isMobile ? '1.1em' : '1.33em' }}>
              To help our planet thrive, Terramatter works on reducing emissions
             using innovative climate friendly technologies
            </p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
    <Segment style={{ padding: '6em 0em', backgroundColor: '#efefef' }} vertical>
      <Grid container stackable verticalAlign='middle' centered>
        <Grid.Row>
          <Grid.Column width={10} textAlign='center'>
            <Header as='h3' style={{ fontSize: isMobile ? '1.5em' : '2em', paddingBottom: isMobile ? '1.25em' : '2em' }}>
            Vision
            </Header>
            <p style={{ fontSize: isMobile ? '1.1em' : '1.33em' }}>
              To strive towards maintaining a habitable &amp; sustainable Earth through bringing about a behavioral
              change in the way we utilize material resources &amp; their byproducts in the production lifecycle thus
              becoming a catalyst of circular carbon economy.
            </p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
    </>
  );
};

export default HomepageLayout;