import PropTypes from 'prop-types'
import React from 'react'
import {
  Container,
  Header,
  Image
} from 'semantic-ui-react'
import ConstructionImage from './../../assets/under_construction.png'

/* Heads up!
 * HomepageHeading uses inline styling, however it's not the best practice. Use CSS or styled
 * components for such things.
 */
const HomepageHeading = ({ mobile }) => (
  <Container text>
    <Header
      as='h1'
      content='TerraMatter'
      inverted
      style={{
        fontSize: mobile ? '2em' : '4em',
        fontWeight: 'normal',
        marginBottom: 0,
        marginTop: mobile ? '2.5em' : '1em',
      }}
    >
      <span style={{color: '#a7c090'}}>Terra</span>
      <span style={{color: '#8b8c8e'}}>Matter</span>
    </Header>
    <div style={mobile ? {
      fontSize: '1.45rem',
      marginTop: '1.5rem'
    } : {
      fontSize: '1.45rem',
      marginTop: '1.5rem'
    }}>We are getting <strong>Ready</strong> !!</div>
    <Image src={ConstructionImage} size="medium" style={{margin: 'auto'}}/>
    <div style={mobile ? {margin: '2rem auto 2rem'} : {margin: '1rem auto 1rem'}}><i>Our website is under construction, but we will be back with something amazing !!</i></div>
  </Container>
)

HomepageHeading.propTypes = {
  mobile: PropTypes.bool,
}

export default HomepageHeading;
