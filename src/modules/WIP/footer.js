import React from 'react';
import {
  Container,
  Grid,
  Header,
  List,
  Segment,
} from 'semantic-ui-react'

const footer = () => {
  return (
    <div>
      <Segment inverted vertical style={{ padding: '5em 0em' }}>
        <Container>
          <Grid divided inverted stackable>
            <Grid.Row>
              <Grid.Column width={3} textAlign="center">
                <Header inverted as='h4' content='Leadership' />
                <List link inverted>
                  <List.Item as='a' href="https://www.linkedin.com/in/shriyanshsonawane/" target="_blank">Shriyansh Sonawane</List.Item>
                </List>
              </Grid.Column>
              <Grid.Column width={3} textAlign="center">
                <Header inverted as='h4' content='Contact Us' />
                <List link inverted>
                  <List.Item as='a'>+91-7718897688</List.Item>
                </List>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </Segment>
    </div>
  );
};

export default footer;