import { createMedia } from '@artsy/fresnel'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Container,
  Menu,
  Segment,
  Visibility,
} from 'semantic-ui-react'

// import HomepageHeading from './HomepageHeading';
import ConstructionPageHeading from './ConstructionPageHeading';
import Footer from './footer';
// import HomepageLayout from './HomepageLayout';

const { MediaContextProvider, Media } = createMedia({
  breakpoints: {
    mobile: 0,
    tablet: 768,
    computer: 1024,
  },
})


/* Heads up!
 * Neither Semantic UI nor Semantic UI React offer a responsive navbar, however, it can be implemented easily.
 * It can be more complicated, but you can create really flexible markup.
 */
class DesktopContainer extends Component {
  state = {}

  hideFixedMenu = () => this.setState({ fixed: false })
  showFixedMenu = () => this.setState({ fixed: true })

  render() {
    const { children } = this.props;

    return (
      <Media greaterThan='mobile'>
        <Visibility
          once={false}
        >
          <Segment
            inverted
            textAlign='center'
            style={{ minHeight: 600, padding: '1em 0em' }}
            vertical
          >
            <Menu
              inverted={true}
              pointing={true}
              secondary={true}
              size='large'
            >
              <Container>
                <Menu.Item as='a'>
                  <span style={{color: '#a7c090'}}>Terra</span>
                  <span style={{color: '#8b8c8e'}}>Matter</span>
                </Menu.Item>
              </Container>
            </Menu>
            <ConstructionPageHeading />
          </Segment>
        </Visibility>
        {children}
      </Media>
    )
  }
}

DesktopContainer.propTypes = {
  children: PropTypes.node,
}

class MobileContainer extends Component {
  state = {}
  render() {
    const { children } = this.props;

    return (
      <Media at="mobile">
        <Segment
          inverted
          textAlign='center'
          style={{ minHeight: 350, padding: '1em 0em' }}
          vertical
        >
          <Container>
            <Menu inverted pointing secondary size='large'>
              <Menu.Item>
                <span style={{color: '#a7c090'}}>Terra</span>
                <span style={{color: '#8b8c8e'}}>Matter</span>
              </Menu.Item>
            </Menu>
          </Container>
          <ConstructionPageHeading mobile />
        </Segment>
        {children}
      </Media>
    )
  }
}

MobileContainer.propTypes = {
  children: PropTypes.node,
}

const ResponsiveContainer = ({ children }) => (
  <MediaContextProvider>
    <DesktopContainer>{children}</DesktopContainer>
    <MobileContainer>{children}</MobileContainer>
  </MediaContextProvider>
)

ResponsiveContainer.propTypes = {
  children: PropTypes.node,
}

const Homepage = () => (
  <ResponsiveContainer>
    {/* <HomepageLayout /> */}
    <Footer />
  </ResponsiveContainer>
)

export default Homepage;