import React from 'react';
import { Segment, Header, Grid, Image } from 'semantic-ui-react';
import Shriyansh from './../../assets/team/Shriyansh.jpg';
import Vilas from './../../assets/team/Vilas.jpg';
import Manju from './../../assets/team/Manju.jpg';

const Team = () => {
  return (
    <div>
      <div id="team" style={{position: "relative", top: "-50px"}}></div>
      <Segment vertical inverted style={{ padding: '4em 4em 2em' }}>
        <Header as='h3' style={{ fontSize: '3em' }}>
          Team
        </Header>
      </Segment>
      <Segment inverted vertical style={{ paddingBottom: '4em' }}>
        <Grid container stackable verticalAlign='middle'>
          <Grid.Row>
            <Grid.Column floated='left' width={6} className='team-card'>
              <Header as='h3' style={{ fontSize: '2em' }} inverted>
                Founder:
              </Header>
              <Image bordered rounded size='small' src={Shriyansh} style={{height: '180px'}}/>
              <Header as='h4' style={{ fontSize: '1.5em' }} inverted>Shriyansh Sonawane</Header>
              <p className='team-card-content'>
                Ex- UPSC Civil Services Aspirant <br/>
                MBA – Symbiosis International University <br/>
                BE – Mumbai University
              </p>
            </Grid.Column>
            <Grid.Column floated='left' width={5} className='team-card'>
              <Header as='h3' style={{ fontSize: '2em' }} inverted>
                Advisors:
              </Header>
              <Image bordered rounded size='small' src={Vilas} style={{height: '180px'}}/>
              <Header as='h4' style={{ fontSize: '1.5em' }} inverted>Dr. Vilas Gaikar</Header>
              <p className='team-card-content'>
                Bharat Petroleum Distinguished Professor in Chemical Engineering <br/>
                Ex Independent BoD- Aarti Drugs Ltd, <br/>
                Coordinator- UGC- National Resource Centre in chemical engineering <br/>
                Research Area: Photochemical reduction of CO2, Thermochemical conversion of Biomass <br/>
                11 Patents &amp; 2 Books published <br/>
                Ph.D., ME &amp; BE - Chemical Engineering
              </p>
            </Grid.Column>
            <Grid.Column floated='left' width={5} className='team-card'>
              <Header as='h3' style={{ fontSize: '2em' }}>
                <span style={{visibility: 'hidden'}}>Dummy</span>
              </Header>
              <Image bordered rounded size='small' src={Manju} style={{height: '180px'}}/>
              <Header as='h4' style={{ fontSize: '1.5em' }} inverted>Dr. Manju Sharma</Header>
              <p className='team-card-content'>
                Ph.D. Industrial Microbiology <br/>
                Research Associate- DBT- ICT center for Energy Biosciences <br/>
                Research Area: Biofuel, Bioenergy &amp; Bio-based chemicals, Waste Valorization, Enzyme production, Purification &amp; characterization <br/>
                1 Patent &amp; 7 Papers Published <br/>
                Research Scholar- Guru Nanak University <br/>
                Ph.D., Microbiology- Guru Nanak Dev University
              </p>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    </div>
  );
};

export default Team;