import React from 'react';
import { Segment, Header, Grid, Image } from 'semantic-ui-react';
import WhatAreWeUpto from './../../assets/WhatAreWeUpto.jpeg';

const AboutUs = () => {
  return (
    <Segment vertical inverted style={{paddingBottom: '2em'}}>
        <Grid container stackable verticalAlign='middle'>
          <Grid.Row>
            <Grid.Column width={8}>
              <Header as='h3' inverted style={{ fontSize: '3em' }}>
                What are we upto?
              </Header>
              <p style={{ fontSize: '1.33em', textAlign: 'justify' }}>
                We are going to revolutionize the way alternative proteins are made using carbon emissions 
                to feed the population of the future.
              </p>
            </Grid.Column>
            <Grid.Column floated='right' width={8}>
              <Image bordered rounded size='large' src={WhatAreWeUpto} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
  );
};

export default AboutUs;