import React from 'react';
import AboutUs from './AboutUs';
import Product from './Product';
import Layout from '../Layout/Layout';
import Heading from './Heading';
import './homepage.css';

const Index = () => {
  return (
    <Layout>
      <Heading />
      <AboutUs />
      <Product />
    </Layout>
  );
};

export default Index;