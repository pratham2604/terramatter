import React from 'react';
import { Segment, Header, Grid, Image } from 'semantic-ui-react';
import WhatAreWeUpto from './../../assets/WhatAreWeUpto.jpeg';

const Product = () => {
  return (
    <>
      <div id="products" style={{position: "relative", top: "-50px"}}></div>
      <Segment vertical style={{ padding: '4em' }}>
        <Header as='h3' style={{ fontSize: '3em' }}>
          Our Products
        </Header>
        <p style={{ fontSize: '1.33em', textAlign: 'justify' }}>
          Our products are an ideal alternate to proteins sources from traditional food & animal farming. 
          These are rich in protein with a balanced amino acid composition. 
          It also provides dietary fiber along with Omega 3 & 6 fatty acids. 
          Its nutritional value, and versatile texture makes it a perfect fit for human and animal consumption.
        </p>
      </Segment>
      <Segment vertical inverted style={{ paddingTop: '4em' }}>
        <ProductContainer src={WhatAreWeUpto} title='Terra Myco'>
          Our mycoprotein is an ideal alternate to proteins sources from traditional food & animal farming. 
          These are rich in protein with a balanced amino acid composition. 
          It also provides dietary fiber along with Omega 3 & 6 fatty acids. 
          Its nutritional value, and versatile texture makes it a perfect fit for human consumption.
        </ProductContainer>
      </Segment>
      <Segment vertical inverted>
        <Grid container stackable verticalAlign='middle'>
          <Grid.Row>
            <Grid.Column width={8}>
              <Header as='h3' style={{ fontSize: '2em' }} inverted>
                Terra Feed 
              </Header>
              <p style={{ fontSize: '1.33em', textAlign: 'justify' }}>
                Provide a valuable substitute to traditional soymeal & fishmeal which leaves a massive carbon footprint 
                in its supply chain. Our feed comprises of essential amino acids like Lysine, Methionine, 
                Threonine, Tryptophan. This feed can provide nutritional fortification to pets & cattle without the 
                need to use 1/3rd of all agricultural land that is currently used to produce Soyabean & maize.
              </p>
            </Grid.Column>
            <Grid.Column floated='left' width={8}>
              <Image bordered rounded size='large' src={WhatAreWeUpto} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
      <Segment vertical inverted style={{ paddingBottom: '4em' }}>
        <ProductContainer src={WhatAreWeUpto} title='Terra Care'>
          Terra-Care are a range of algae based bioactive compounds providing peptides & antioxidants for skin care. 
          They also provide hydration to the skin & has anti aging properties along with treatment for pigmentation. 
          All of these are organic & produced in a carbon neutral processes unlike the existing chemicals derived from crude 
          oil & carbon intensive fossil energy.
        </ProductContainer>
      </Segment>
    </>
  );
};

const ProductContainer = ({ src, title, children }) => (
  <Grid container stackable verticalAlign='middle'>
    <Grid.Row>
      <Grid.Column floated='left' width={8}>
        <Image bordered rounded size='large' src={src} />
      </Grid.Column>
      <Grid.Column width={8}>
        <Header as='h3' style={{ fontSize: '2em' }} inverted>
          {title}
        </Header>
        <p style={{ fontSize: '1.33em', textAlign: 'justify' }}>
          {children}
        </p>
      </Grid.Column>
    </Grid.Row>
  </Grid>
)

export default Product;