import { Header, Segment } from 'semantic-ui-react'

const HomepageHeading = ({ mobile }) => (
  <Segment inverted className='home-heading-container'>
    <div className='homepage-heading-cover'>
      <Header as='h1' inverted className='home-heading-title'>
        <div style={{width: '70%'}}>
          Harnessing Carbon to Enable Circular Economy
        </div>
      </Header>
      <Header as="h2" inverted className='homepage-heading-cover-content'>
        <div style={{width: '40%', paddingRight: '5rem'}}>
        Terramatter aims at putting Carbon emissions to use to make critical & specialty chemicals 
        like Single cell Proteins (SCP) thereby creating a positive spiral of circular carbon economy.
        </div>
      </Header>
    </div>
  </Segment>
)

export default HomepageHeading;