import React from 'react';
import { Segment, Header } from 'semantic-ui-react';

const Technology = () => {
  return (
    <div className='homepage-layout-heading-cover-container'>
      <Segment className='homepage-layout-heading-cover'>
        <div className='homepage-layout-heading-cover-content'>
          <Header as='h2' className='homepage-layout-heading-cover-content-title'>
            Technology
          </Header>
          <p style={{ fontSize: '1.33em', width: '900px', textAlign: 'justify' }}>
            Terramatter’s flagship product Promatter is a form of Mycoprotein. 
            It is derived from generally recognized as Safe (GRAS) organisms which are grown in a carbon negative process. 
            We call this process ALTMATTER. Altmatter is a gaseous fermentation process that uses carbon dioxide as 
            feedstock to be fed to the fermentation chambers. 
            This Co2 undergoes a 2 stage process in which it gets converted to substrate for the GRAS organisms which grows 
            in biomass there by forming the Mycoprotein.
          </p>
        </div>
      </Segment>
    </div>
  );
};

export default Technology;