import React from 'react';
import { Segment, Header, Grid, Image } from 'semantic-ui-react';
import GFI from './../../assets/partnerships/gfi.png';
import INNOVATE from './../../assets/partnerships/innovate.png';

const Partnerships = () => {
  return (
    <div>
      <div vertical style={{ padding: '4em 4em 0' }}>
        <Header as='h3' style={{ fontSize: '3em' }}>
          Partnerships
        </Header>
      </div>
      <Segment vertical>
        <Grid container stackable verticalAlign='middle'>
          <Grid.Row>
            <Grid.Column floated='left' width={8}>
              <Image rounded size='medium' src={GFI} style={{margin: 'auto'}} />
            </Grid.Column>
            <Grid.Column floated='left' width={8}>
              <Image rounded size='medium' src={INNOVATE} style={{margin: 'auto'}} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    </div>
  );
};

export default Partnerships;