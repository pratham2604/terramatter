import React from 'react';
import { Segment, Header, Grid, Image } from 'semantic-ui-react';
import WhatAreWeUpto from './../../assets/WhatAreWeUpto.jpeg';

const Impact = () => {
  return (
    <div>
      <div id="impact" style={{position: "relative", top: "-50px"}}></div>
      <Segment vertical style={{ padding: '4em' }} inverted>
        <Header as='h3' style={{ fontSize: '3em' }}>
          Impact
        </Header>
        <p style={{ fontSize: '1.33em' }}>
          Our alternative proteins will gradually displace proteins that are otherwise sourced 
        </p>
      </Segment>
      <Segment vertical>
        <Grid container stackable verticalAlign='middle'>
          <Grid.Row>
            <Grid.Column floated='left' width={8}>
              <Image bordered rounded size='large' src={WhatAreWeUpto} />
            </Grid.Column>
            <Grid.Column floated='left' width={8}>
              <Image bordered rounded size='large' src={WhatAreWeUpto} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    </div>
  );
};

export default Impact;