import { Grid, Segment, Image, Header } from 'semantic-ui-react';
import Shriyansh from './../../assets/team/Shriyansh.jpg';
import Vilas from './../../assets/team/Vilas.jpg';
import Manju from './../../assets/team/Manju.jpg';

const Team = () => (
  <Grid.Row>
    <Grid.Column width={14} className='info-container'>
      <div className='info-title poppins-font small-title'>Team</div>
      <Segment vertical style={{ paddingBottom: '4em' }}>
        <Grid container stackable verticalAlign='top'>
          <Grid.Row centered>
            <Grid.Column width={6} className='team-card'>
              <Header as='h3' style={{ fontSize: '2em' }} className='poppins-font'>
                Founder:
              </Header>
              <Image bordered rounded size='small' src={Shriyansh} style={{height: '180px'}}/>
              <Header as='h4' style={{ fontSize: '1.5em' }} className='poppins-font'>Shriyansh Sonawane</Header>
              <p className='team-card-content poppins-font'>
                Ex- UPSC Civil Services Aspirant <br/>
                MBA – Symbiosis International University <br/>
                BE – Mumbai University
              </p>
            </Grid.Column>
            <Grid.Column width={5} className='team-card'>
              <Header as='h3' style={{ fontSize: '2em' }} className='poppins-font'>
                Advisors:
              </Header>
              <Image bordered rounded size='small' src={Vilas} style={{height: '180px'}}/>
              <Header as='h4' style={{ fontSize: '1.5em' }} className='poppins-font'>Dr. Vilas Gaikar</Header>
              <p className='team-card-content poppins-font'>
                Bharat Petroleum Distinguished Professor in Chemical Engineering <br/>
                Ex Independent BoD- Aarti Drugs Ltd, <br/>
                Coordinator- UGC- National Resource Centre in chemical engineering <br/>
                Research Area: Photochemical reduction of CO2, Thermochemical conversion of Biomass <br/>
                11 Patents &amp; 2 Books published <br/>
                Ph.D., ME &amp; BE - Chemical Engineering
              </p>
            </Grid.Column>
            <Grid.Column width={5} className='team-card'>
              <Header as='h3' style={{ fontSize: '2em' }}>
                <span style={{visibility: 'hidden'}}>Dummy</span>
              </Header>
              <Image bordered rounded size='small' src={Manju} style={{height: '180px'}}/>
              <Header as='h4' style={{ fontSize: '1.5em' }} className='poppins-font'>Dr. Manju Sharma</Header>
              <p className='team-card-content poppins-font'>
                Ph.D. Industrial Microbiology <br/>
                Research Associate- DBT- ICT center for Energy Biosciences <br/>
                Research Area: Biofuel, Bioenergy &amp; Bio-based chemicals, Waste Valorization, Enzyme production, Purification &amp; characterization <br/>
                1 Patent &amp; 7 Papers Published <br/>
                Research Scholar- Guru Nanak University <br/>
                Ph.D., Microbiology- Guru Nanak Dev University
              </p>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    </Grid.Column>
  </Grid.Row>
);

export default Team;